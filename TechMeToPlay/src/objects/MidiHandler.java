/*
 * Responsible for setting up all Midi configurations
 */

package objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;
import javax.swing.JTextArea;

//import objects.MidiHandler.MidiInputReceiver;

public class MidiHandler
{
	
	//These things handle sound output
	private MidiChannel[] channels;
	private Synthesizer midiSynth;
	private Instrument[] instr;
	
	//Practically vestigial
	private JTextArea txtrInputUsingMidi;
	
	public boolean recording = false;
	
	public long recordingTimeStart = 0;
	public long tempo;
	public double division = 0; //Time in the recording
	
	private double complexity; //.25 means divisions on quarter notes, .5 means it can only play half notes
	//private int loudnessSteps = 3; //How many different playback volumes will we hear?
	
	public HashMap<Integer, ArrayList<Note>> notes; //Stored values enter here as we recieve midi input
	public HashMap<Integer, String> noteMonitor;
	
	
	//Here we initialize everything related to MIDI in this project
	public MidiHandler(JTextArea textArea, long newTempo, double newComplexity)
	{
		txtrInputUsingMidi = textArea;
		tempo = newTempo;
		complexity = newComplexity;
		
		MidiDevice midi = null;
		//taps = new ArrayList<Long>();
		notes = new HashMap<Integer, ArrayList<Note>>();
		noteMonitor = new HashMap<Integer, String>();
		
		try
		{
			midiSynth = MidiSystem.getSynthesizer();
			midiSynth.open();
			midi = getMidiDevice();
		}
		catch (Exception e)
		{
			System.err.println("Error");
			e.printStackTrace();
		}
		
		instr = midiSynth.getDefaultSoundbank().getInstruments();
		midiSynth.loadInstrument(instr[0]);//load an instrument
        channels = midiSynth.getChannels();
	}
	
	//To be called by the main program every tempo * complexity milliseconds
	public void incrementDivision(int inc)
	{
		division += inc;
	}
	
	//Takes a note and will play it through the computer's speakers
	public long playNote(Note note)
	{
		new Thread(new Runnable() { //Play the note asynchronously so we can play chords as well
		    public void run()
		    {
		    	channels[0].noteOn(note.note, note.velocity);
		    	try
		    	{
		    		Thread.sleep(note.getDurationMS(tempo, complexity));
		    	}
		    	catch (Exception e) { e.printStackTrace(); }
		        channels[0].noteOff(note.note);
		    }
		}).start();
		return (int) ((tempo * note.duration) * complexity);
	}
	
	//Just resets an arraylist (instead of reallocating it). Not called often
	public void empty(HashMap<Integer, ArrayList<Note>> record)
	{
		if (record == null) return;
		int i=0;
		while (record.containsKey(i))
		{
			while (record.get(i).size() > 0)
			{
				record.get(i).remove(0);
			}
			i++;
		}
	}
	
	//All notes we read from MIDI get passed here after we stop recording
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void analyzeData(HashMap<Integer, ArrayList<Note>> data)
	{
		ArrayList<String> recordedNotes = new ArrayList<String>();
		
		List sortedKeys=new ArrayList(data.keySet());
		Collections.sort(sortedKeys);
		for (Object index : sortedKeys)
		{
			Integer i = (Integer) index;
			//System.out.println("Got time interval: " + i);
			for (Note n : data.get(i))
			{
				//System.out.println("\tNote: " + n.toDataString(tempo, complexity));
				recordedNotes.add(n.toDataString(tempo, complexity));
			}
		}
		
		//Convert the input into a time-wise linear array of notes. 
		
		//Build our Markov model from the input
		MarkovChain mc = new MarkovChain();
		mc.createModel(recordedNotes);
		
		//Use the markov model to generate 
		ArrayList<Chord> output = mc.writeMusic(1000);
		
		if (output == null)
		{
			return;
		}
		
		//The markov generator returns chords. Here we play them all asynchronously and make sure timings are still accurate
		for (Chord chord : output)
		{
			//playNotes(chord);
			long delay = 0;
			for (String s : chord.notes)
			{
				Note note = new Note(s);
				if (note.toString().split(";")[1].equals("64")) //.equals("64")
				{
					//pass: this note is broken on the keyboard in room B30 in Alden!
				}
				else
				{
			    	delay = Math.max(delay, playNote(note));
				}
			}
			
			try
			{
				Thread.sleep(delay);
			}
			catch (Exception e) {System.err.println("failed");}
		}
	}
	
	//Sets up the midi input device
	private MidiDevice getMidiDevice()
	{
		MidiDevice device = null;
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		
		for (int i = 0; i < infos.length; i++)
		{
            try
            {
            	device = MidiSystem.getMidiDevice(infos[i]);
            	//does the device have any transmitters?
            	//if it does, add it to the device list
            	System.out.println(infos[i]);

            	//get all transmitters
            	List<Transmitter> transmitters = device.getTransmitters();
            	//and for each transmitter

            	for(int j = 0; j<transmitters.size();j++)
            	{
            		//create a new receiver
            		transmitters.get(j).setReceiver(new MidiInputReceiver(device.getDeviceInfo().toString()));
            	}

            	Transmitter trans = device.getTransmitter();
            	trans.setReceiver(new MidiInputReceiver(device.getDeviceInfo().toString()));

            	//open each device
            	device.open();
            	//if code gets this far without throwing an exception
            	//print a success message
            	System.out.println(device.getDeviceInfo()+" Was Opened");
            	String deviceName = device.getDeviceInfo().toString();
            	
            	//WHITELIST any addition devices here!
            	if (!deviceName.contains("Axiom") && !deviceName.contains("CASIO USB-MIDI"))
            	{
            		throw new MidiUnavailableException();
            	}
            }
            catch (MidiUnavailableException e)
            {
            	System.err.println("Error finding midi device!");
            }
		}
		
		return device;
	}
	
	//Handles all midi input
	private class MidiInputReceiver implements Receiver
	{
	    public String name;
	    
	    public final int NOTE_OFF = 0x80;
	    public final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	    
	    public MidiInputReceiver(String newName)
	    {
	        this.name = newName;
	    }
	    
	    //Callback for when we recieve a midi message 
	    public void send(MidiMessage msg, long timeStamp)
	    {
	    	ShortMessage data = (ShortMessage) msg;
	    	Note notePlayed = new Note(data);
	    	
	    	//Used to have this section for printing stuff but I got rid of it for speed
	    	/*int key = data.getData1();
            int octave = (key / 12)-1;
            int note = key % 12;
            String noteName = NOTE_NAMES[note];
            int velocity = data.getData2();
	    	
	    	String output = "Channel: " + data.getChannel() + ", Note: " + noteName + octave + ", Pitch: " + key + ", Velocity: " + velocity;
	    	*/
	    	
	    	if (data.getCommand() == NOTE_OFF || data.getData2() == 0) //Velocity is 0
	    	{
	    		//channels[0].noteOff(notePlayed.note); //Uncomment to have your computer play input notes
	    		
	    		//output = "Off";
	    		if (recording)
	    		{
	    			//If we are recording then mark that we hit this note.
	    			
	    			if (!notes.containsKey((int)division))
	    			{
	    				notes.put((int)division, new ArrayList<Note>());
	    			}
	    			
	    			long noteStartTime = 0;
	    			int noteVelocity = 0;
	    			try
	    			{
	    				String noteSaveData[] = noteMonitor.get(notePlayed.hashCode()).split(";");
	    				noteStartTime = Long.parseLong(noteSaveData[0]);
	    				noteVelocity = Integer.parseInt(noteSaveData[1]);
	    			}
	    			catch (Exception e)
	    			{
	    				e.printStackTrace();
	    			}
	    			
	    			notePlayed.setStartTime(noteStartTime);
	    			notePlayed.setEndTime(System.currentTimeMillis());
	    			notePlayed.setVelocity(noteVelocity);
	    			notePlayed.setDivision((int)division);
	    			notes.get((int)division).add(notePlayed);
	    			//System.out.println(notePlayed.toString() + "-");
	    			//notes.get((int)division).add(data);
	    		}
	    	}
	    	else //Keeps track of note presses. We log new notes only after the key comes up
	    	{
	    		//channels[0].noteOn(notePlayed.note, notePlayed.velocity); //Uncomment to have your computer play input notes
	    		noteMonitor.put(notePlayed.hashCode(), System.currentTimeMillis() + ";" + notePlayed.velocity);
	    		//txtrInputUsingMidi.append("\n" + output);
	    	}
	        
	    }
	    public void close()
	    {
	    }
	}
}

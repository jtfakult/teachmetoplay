/*
 * Object that keeps track of all chords that are entered through midi.
 * Stores strings that will be parsed by the Note Object
 */

package objects;

import java.util.ArrayList;

@SuppressWarnings("rawtypes")
public class Chord implements Comparable
{
	ArrayList<String> notes;
	
	public Chord()
	{
		notes = new ArrayList<String>();
	}
	
	public Chord(ArrayList<String> n)
	{
		notes = n;
	}
	
	public void add(String s)
	{
		notes.add(s);
	}
	
	public String get(int i)
	{
		return notes.get(i);
	}
	
	public ArrayList<String> getNotes()
	{
		return notes;
	}
	
	public String getFirstNote()
	{
		if (notes.size() > 0)
		{
			return notes.get(0);
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public boolean equals(Object c2)
	{
		return this.notes.equals(((Chord) c2).getNotes());
	}

	@Override
	public int compareTo(Object o)
	{
		Chord c = (Chord) o;

		Note n1 = new Note(notes.get(0));
		Note n2 = new Note(c.getFirstNote());
		
		return n1.compareTo(n2);
	}
}

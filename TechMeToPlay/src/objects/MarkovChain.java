/*
 * Builds the model and has an algorithm for generation as well
 */

package objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class MarkovChain
{
	private final int DEPTH = 4; //How deep is the statistical chaining stored (hidden levels)?
	
	private HashMap<ArrayList<Chord>, HashMap<Chord, Integer>> probabilities; //Our ridiculous data structure to keep track of the model
	
	private SortedSet<Chord> uniqueNotes;
	
	public MarkovChain()
	{
		super();
		probabilities = new HashMap<ArrayList<Chord>, HashMap<Chord, Integer>>();
		uniqueNotes = new TreeSet<Chord>();
	}
	
	//Load in a linear arraylist of strings (that are formatted notes)
	public void createModel(ArrayList<String> allNotes)
	{
		System.out.println("Optimizing MIDI input...");
		ArrayList<Chord> notes = new ArrayList<Chord>();
		
		//Converts the linear arraylist into a linear arraylist of chords based on starting times of notes
		//The rest of the model deals with chords instead of notes from now onwards
		for (String s : allNotes)
		{
			boolean foundSameChord = false;
			for (int i=0; i<notes.size(); i++)
			{
				
				Chord ch = notes.get(i);
				String chordStart = ch.get(0).split(";")[2];
				String newNoteStart = s.split(";")[2];
				if (chordStart.equals(newNoteStart))
				{
					foundSameChord = true;
					notes.get(i).add(s);
				}
			}
			
			if (!foundSameChord)
			{
				Chord c = new Chord();
				c.add(s);
				notes.add(c);
			}
		}
		
		System.out.println("Loading model");
		
		for (Chord s : notes)
		{
			uniqueNotes.add(s);
		}
		
		if (notes.size() < 1)
		{
			System.err.println("No notes given!");
			return;
		}
		
		//do all the crazy analysis stuff that puts chords as a key with an arraylist of chords following in terms of frequencies.
		System.out.println("Building Model");
		//ArrayList<String> key = new ArrayList<String>();
		for (int level=0; level<Math.min(notes.size(), DEPTH); level++)
		{
			int maxIndex = Math.max(1, notes.size()-level);
			
			for (int position = 0; position < maxIndex; position++)
			{
				ArrayList<Chord> node = new ArrayList<Chord>(level+1);
				
				//Initialize the key in this hashmap to be the first 'level' notes
				for (int i=position; i<position+level+1; i++)
				{
					node.add(notes.get(i));
				}
				
				Chord nextNote = null;
				if (position+level+1 >= notes.size())
				{
					System.out.println("End of notes!");
					nextNote = notes.get(0);
				}
				else
				{
					nextNote = notes.get(position+level+1);
				}
				
				if (!probabilities.containsKey(node))
				{
					HashMap<Chord, Integer> newProb = new HashMap<Chord, Integer>();
					newProb.put(nextNote, 0);
					probabilities.put(node, newProb);
				}
				if (!probabilities.get(node).containsKey(nextNote))
				{
					HashMap<Chord, Integer> newProb = new HashMap<Chord, Integer>();
					newProb.put(nextNote, 0);
					probabilities.get(node).put(nextNote, 0);
					//probabilities.get(node).put(nextNote, 0);
				}
				
				int currentCount = probabilities.get(node).get(nextNote);
				probabilities.get(node).put(nextNote, currentCount + 1);
			}
			
			
			/*System.out.println("Printing Model:");
			for (ArrayList<String> s : probabilities.keySet())
			{
				System.out.println("Key: ");
				for (String ss : s)
				{
					System.out.print(ss + ", ");
				}
				System.out.println();
				
				System.out.println("Probabilities for key:");
				HashMap<String, Integer> value = probabilities.get(s);
				System.out.println("Length: " + value.size());
				for (String note : value.keySet())
				{
					System.out.println("Note: " + note + ": " + probabilities.get(s).get(note));
				}
				System.out.println("---");
			}*/
		}
	}
	
	/*private void empty(ArrayList<String> a)
	{
		while (a.size() > 0)
		{
			a.remove(0);
		}
	}*/
	
	//Wrapper
	public ArrayList<Chord> writeMusic(int numNotes)
	{
		return writeMusicAlgo2(numNotes);
	}
	
	//Markov generation algorithm. Not gonna explain how it works...
	//Takes number of notes and uses the probabilities model to generate output
	private ArrayList<Chord> writeMusicAlgo2(int numNotes)
	{
		ArrayList<Chord> notes = new ArrayList<Chord>();
		//int randomizerSize = uniqueNotes.size() * 2;
		double weights[] = new double[DEPTH];
		
		//Assign weights to the different levels
		for (int i=0; i<DEPTH; i++)
		{
			weights[i] = (i*.5+.5);
		}
		
		Chord firstNote = chooseStartingNote();
		if (firstNote == null)
		{
			System.err.println("Starting note was null... well that shouldn't happen!");
			//System.exit(0);
			return null;
		}
		
		System.out.println("First note: " + firstNote);
		
		//ArrayList<String> notesSoFar = new ArrayList<String>();
		notes.add(firstNote);
		
		for (int i=0; i<numNotes; i++)
		{
			int targetDepth = (int) Math.random() * Math.min(notes.size(), DEPTH) + 1;
			//int targetDepth = (int) Math.min(notes.size(), chooseTargetDepth(weights));
			//System.out.println(targetDepth + "---");
			int noteSize = notes.size(); 
			HashMap<Chord, Integer> potentialNotes = probabilities.get(notes.subList(noteSize-targetDepth, noteSize));
			
			while (potentialNotes.size() == 0)
			{
				targetDepth--;
				
				//Theoretically this should never give an index out of bounds error
				potentialNotes = probabilities.get(notes.subList(noteSize-targetDepth, noteSize)); //Current note
			}
			
			//Choose a random note. Might decide to not be lazy and optimize this because this is shit code that may hang. yeah...
			/*int randomIndex = (int) (Math.random() * potentialNotes.size());
			while (!probabilities.containsKey(potentialNotes.get(randomIndex)))
			{
				//This loop should never run but I;m keeping it in since it was part of an older design
				randomIndex = (int) (Math.random() * potentialNotes.size());
			}*/
			
			//ArrayList<String> keyValue = new ArrayList<String>();
			//keyValue.add(index);
			
			int sum = 0;
			for (Chord key : potentialNotes.keySet())
			{
				sum += potentialNotes.get(key);
			}
			
			int randomWeight = (int) (Math.random() * sum * 2);
			
			boolean foundOne = false;
			while (randomWeight >= 0)
			{
				for (Chord key : potentialNotes.keySet())
				{
					if (foundOne) //This may be the ugliest case I've ever wrote
					{
						continue;
					}
					randomWeight -= potentialNotes.get(key);
					if (randomWeight < 0)
					{
						notes.add(key);
						foundOne = true;
					}
				}
			}
			//System.out.println(notes.size());
		}
		
		return notes;
	}
	
	/*private int chooseTargetDepth(double weights[])
	{
		double sum = 0;
		for (double i : weights)
		{
			sum += i;
		}
		
		double randomValue = Math.random() * sum * 2;
		for (int index = 0; ; index = (index + 1) % weights.length)
		{
			randomValue -= weights[index];
			if (randomValue < 0)
			{
				return index;
			}
		}
	}*/
	
	//Picks a starting to to play first (semi-randomly). Then the generator works from there.
	private Chord chooseStartingNote()
	{
		Set<ArrayList<Chord>> notes = probabilities.keySet();
		
		
		int size = notes.size();
		
		if (size == 0)
		{
			System.err.println("No keys given");
			return null;//System.exit(0);
		}
		
		int index = (int) (Math.random() * size);
		for (ArrayList<Chord> n : notes)
		{
			if (index == 0)
			{
				return n.get(0);
			}
			index--;
		}
		
		System.out.println("Index was only: " + index);

		return null;
	}
	
	//Old function used to compact notes into chords. Moved this to the top of createModel though.
	/*private ArrayList<ArrayList<String>> compact(ArrayList<String> notes)
	{
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		
		ArrayList<String> chord = new ArrayList<String>();
		boolean chordLast = true;
		int startTime = 0;
		for (int i=0; i<notes.size(); i++)
		{
			if (chord.size() == 0)
			{
				chord.add(notes.get(i));
				startTime = Integer.parseInt(notes.get(i).split(";")[2]);
				continue;
			}
			
			String newNote = notes.get(i);
			int newStartTime = Integer.parseInt(newNote.split(";")[2]);
			
			if (startTime == newStartTime)
			{
				chord.add(newNote);
				chordLast = true;
			}
			else
			{
				result.add(chord);
				chord = new ArrayList<String>();
				chordLast = false;
			}
		}
		
		if (chordLast)
		{
			result.add(chord);
		}
		
		return result;
	}*/
}


/*

Three algorithms for output:
Getting the starting note:


--Algo 1:
1. Give weight to each level based on total amount of notes recorded vs. how "even" the model is with regards to the next step.
2. Give weighting to higher layers which we may see less total amounts of notes.

--Algo 2:
1. Completely random choices between any level. Maybe add weighting to certain levels?
2. If we add weighting it can at least be customizable

--Algo 3:
1. Basically the same as 2 but we add weighting to levels based on a sin wave over time.
2. Higher sign values correlate to weighting towards higher layers.
*/
/*
 * Represents a single note to be played through the speaker.
 * Handles all data formatting, etc.
 */

package objects;

import javax.sound.midi.ShortMessage;

@SuppressWarnings("rawtypes")
public class Note implements Comparable
{
	int channel;
	int note;
	int velocity;
	
	long startTime;
	long endTime;
	
	int duration;
	
	int velocityIntervals;
	
	int division;
	
	public Note(ShortMessage msg)
	{
		channel = msg.getChannel();
		note = msg.getData1();
		velocity = msg.getData2();
		
		startTime = 0;
		endTime = 0;
		
		duration = 0;
		
		velocityIntervals = 8;
		
		division = 0;
	}
	
	public Note(String encodedNote)
	{
		String parts[] = encodedNote.split(";");
		if (parts.length == 5)
		{
			channel = Integer.parseInt(parts[0]);
			note = Integer.parseInt(parts[1]);
			division = Integer.parseInt(parts[2]);
			velocity = Integer.parseInt(parts[3]);
			
			startTime = 0;
			duration = Integer.parseInt(parts[4]);
		}
		else
		{
			System.err.println("Invalid note");
		}
	}
	
	public void setStartTime(long newStartTime)
	{
		startTime = newStartTime;
	}
	
	public void setEndTime(long newEndTime)
	{
		endTime = newEndTime;
	}
	
	public void setVelocity(int newVelocity)
	{
		velocity = newVelocity;
	}
	
	public void setVelocityIntervals(int newVelocityIntervals)
	{
		velocityIntervals = Math.max(1, newVelocityIntervals);
	}
	
	public String toString()
	{
		return channel + ";" + note + ";" + velocity + ";" + startTime + ";" + endTime;
	}
	
	/*private String pad(int value, int paddingLength)
	{
		String result = "" + value;
		while (result.length() < paddingLength)
		{
			result += "-";
		}
		
		return result;
	}*/
	
	public void markEndTime()
	{
		endTime = System.currentTimeMillis();
	}
	
	public long getDuration()
	{
		return startTime - endTime;
	}
	
	public int getDurationMS(long tempo, double intervalSize)
	{
		return (int) (tempo * intervalSize * duration);
	}
	
	@Override
	public boolean equals(Object note2)
	{
		System.out.println(toString());
		Note n2 = (Note) note2;
		System.out.println("--" + n2.toString());
		return channel == n2.channel &&
				note == n2.note &&
				velocity == n2.velocity &&
				startTime == n2.startTime &&
				endTime == n2.endTime;
	}
	
	@Override
	public int hashCode()
	{
	    return (channel + ";" + note + ";0;" + startTime + ";" + endTime).hashCode();
	}
	
	public int getIntervalDuration(long tempo, double intervalSize)
	{
		long tempos = (int)((endTime-startTime) + tempo / 2);
		tempos = (int) (tempos / intervalSize);
		tempos /= tempo;
		return (int) tempos;
	}
	
	private int getVelocityInterval()
	{
		int maxVelocity = 128;
		int stepSize = maxVelocity / velocityIntervals;
		for (int i = stepSize; i <= maxVelocity; i += stepSize)
		{
			if (velocity <= i)
			{
				return i;
			}
		}
		
		return -1;
	}
	
	public void setDivision(int newDivision)
	{
		division = newDivision;
	}

	@Override
	public int compareTo(Object secondNote)
	{
		return (int) (startTime - ((Note)secondNote).startTime);
	}
	
	public String toDataString(long tempo, double intervalSize)
	{
		
		return channel + ";" + note + ";" + division + ";" + getVelocityInterval() + ";" + getIntervalDuration(tempo, intervalSize);
	}
	
	//public static 
}

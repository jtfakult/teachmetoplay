# README #

Feel free to pull from my public repository at:
https://jtfakult@bitbucket.org/jtfakult/teachmetoplay.git

One IMPORTANT thing to note:
Any MIDI input you attach may not be read by this program UNLESS IT IS WHITELISTED in the source code on line 213 of the file MidiHandler.java (below the line of code that says: "//WHITELIST any addition devices here!"

The two whitelisted devices currently are the small keyboards and the large keyboard in room B30 in Alden Hall.
//tetris @ 150
//Improv with sweet child left hand
//Beethovens 5th

package view;

import java.awt.Color;
import java.awt.EventQueue;


import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.DefaultCaret;

import objects.Chord;
import objects.MarkovChain;
import objects.MidiHandler;
import objects.Note;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.swing.JToggleButton;
import javax.swing.JButton;

public class MainScreen
{
	private JFrame frame;
	
	private static JTextArea txtrInputUsingMidi;
	private static JButton tempoButton;
	
	private static MidiHandler mh;
	
	private static ArrayList<Long> taps;
	private static int tapAverageSize = 15;
	
	private static long tempo = 500;
	private static double complexity = .5; //.25 means divisions on quarter notes, .5 means it can only play half notes
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MainScreen window = new MainScreen();
					window.frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
		
		//Create and start the midi handler, settings all all of our input stuff
		mh = new MidiHandler(txtrInputUsingMidi, tempo, complexity);
		taps = new ArrayList<Long>(); //Handles when we manually set the tempo
	}

	//Plays a system beep in time of the tempo... Lol I'm lazy
	//The tempo also handles the updating of the global division (i.e time since we started recording) <-- VERY important
	private static void playTempo()
	{
		//long tick = (long) (tempo * complexity);
		final int flashTime = 50; 
		
		tempoButton.setOpaque(true);
		tempoButton.setBorderPainted(false);
		
		new Thread(new Runnable()
		{
		    public void run()
		    {
		    	while (true)
		    	{
		    		long tick = (long) (tempo);
		    		try
		    		{
		    			long numIncrements = (long) ((tick-flashTime) / (tick * complexity));
		    			long remainder = (tick-flashTime) % (long)(tick * complexity);
		    			for (int i=0; i<numIncrements; i++)
		    			{
		    				Thread.sleep((long)(tick * complexity));
		    				mh.incrementDivision(1);
		    			}
		    			Thread.sleep(remainder);
		    			
		    			tempoButton.setBackground(new Color(238, 200, 200));
		    			if (mh.recording)
		    			{
		    				Toolkit.getDefaultToolkit().beep();
		    			}
		    			Thread.sleep(flashTime);
		    			tempoButton.setBackground(new Color(238, 238, 238));
		    			mh.incrementDivision(1);
		    		}
		    		catch (Exception e) {System.err.println("error here");}
		    	}
		    }
		}).start();
	}

	/**
	 * Create the application.
	 */
	public MainScreen()
	{
		initialize();
		playTempo();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame("TeachMeToPlay!");
		frame.setBounds(100, 100, 650, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(4, 1, 0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		
		JLabel lblIAmRecording = new JLabel("I am recording your MIDI input");
		panel.add(lblIAmRecording);
		
		txtrInputUsingMidi = new JTextArea();
		txtrInputUsingMidi.setText("Input using midi...");
		txtrInputUsingMidi.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane();
		DefaultCaret caret = (DefaultCaret)txtrInputUsingMidi.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		JToggleButton recordButton = new JToggleButton("Record");
		recordButton.addItemListener(new ItemListener() //Determine if we are recording or not and change things accordingly
		{												//When we stop recording, immediately analyze the data 
			@SuppressWarnings("unchecked")
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					mh.empty(mh.notes);
					mh.recording = true;
					mh.recordingTimeStart = System.currentTimeMillis();
				}
				else
				{
					mh.recording = false;
					if (mh.notes == null)
					{
						System.err.println("No notes recorded!");
						return;
					}
					for (Integer index : mh.notes.keySet())
					{
						Collections.sort(mh.notes.get(index));
					}
					
					mh.analyzeData(mh.notes);
				}
			}
			
		});
		frame.getContentPane().add(recordButton);
		
		tempoButton = new JButton("Tap me to get the tempo");
		tempoButton.addActionListener(new ActionListener() //Here to let the user adjust the tempo. Its crappy code but it works
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				taps.add(System.currentTimeMillis());
				
				if (taps.size() == 1)
				{
					return;
				}
				
				while (taps.size() > tapAverageSize)
				{
					taps.remove(0);
				}
				
				long average = 0;
				for (int i=1; i<taps.size(); i++)
				{
					average += taps.get(i) - taps.get(i-1);
				}
				System.out.println(average + " " + taps.size());
				tempo = average / (taps.size() - 1);
				average = (60 * 1000) / tempo;
				
				System.out.println("Tempo: " + average);
				mh.tempo = tempo;
			}
		});
		frame.getContentPane().add(tempoButton);
		
		frame.getContentPane().add(scrollPane);
		scrollPane.setViewportView(txtrInputUsingMidi);
	}
}

